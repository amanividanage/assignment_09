
/*Create a file named "assignment9.txt" and open it in write mode. Write the sentence "UCSC is one of the leading institutes in Sri Lanka for computing studies." to the file.

Open the file in read mode and read the text from the file and print it to the output screen.

Open the file in append mode and append the sentence "UCSC offers undergraduate and postgraduate level courses aiming a range of computing fields." to the file.*/
#include <stdio.h>
int main(){
	FILE*fp;
	char str[100];
	fp=fopen("assignment09.txt","w");
	fprintf(fp,"UCSC is one of the leading institutes in Sri Lanka for computing studies.\n");
	fclose(fp);
	fp=fopen("assignment09.txt", "r");

 if( fgets (str, 100, fp)!=NULL ) {
      /* writing content to stdout */
      puts(str);
   }
	fclose(fp);
	fp=fopen("assignment09.txt", "a");
	fprintf(fp,"UCSC offers undergraduate and postgraduate level courses aiming a range of computing fields.");
	fclose(fp);
	
	return 0;
}